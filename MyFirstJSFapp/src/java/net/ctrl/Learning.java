package net.ctrl;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

@ManagedBean(name = "obj")
@SessionScoped

public class Learning 
{
    private Part file;
    
    public void print()
    {
        try
        {
            InputStream input=file.getInputStream();
            File f=new File("/Users/Rijwan Hossain/Desktop/upload/image.jpg");
            
            if(!f.exists())
            {
                f.createNewFile();
            }
            FileOutputStream output=new FileOutputStream(f);
            
            byte[] buffer=new byte[1024];
            int length;
            
            while((length=input.read(buffer))>0)
            {
                output.write(buffer, 0, length);
            }
            
            input.close();
            output.close();
        }
        catch(Exception e)
        {
            e.printStackTrace(System.out);
        }
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }
}
